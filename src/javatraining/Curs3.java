/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatraining;

import java.util.Scanner;

/**
 *
 * @author Vitaliy
 */
public class Curs3 {
    
    private static final int CURS_NUMBER = 3;
    
    public Curs3(){

        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 1);
        curs3_ex1();

        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 2);
        curs3_ex2();

        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 3);
        curs3_ex3();

        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 4);
        curs3_ex4();

        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 5);
        curs3_ex5();

        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 6);
        curs3_ex6();

        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 7);
        curs3_ex7();

        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 8);
        curs3_ex8();

        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 9);
        curs3_ex9();

        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 10);
        curs3_ex10();
    }
    
    /**
     * Exercise:
    * Convert the following for loop statement to a while loop and to a do-while loop.  
    * long sum = 0;
    * for ( int i = 0; i <= 1000; i++ )
    * {  
    *   sum = sum + i;
    * }
     */
    public static void curs3_ex1(){
        long sum = 0;
        for ( int i = 0; i <= 1000; i++ ){  
            sum = sum + i;
        }
        
        long sum2 = 0;
        int i2 = 0;
        while(i2 <= 1000){
            sum2 = sum2 + i2;
            i2++;
        }
        
        long sum3 = 0;
        int i3 = 0;
        do{
           sum3 = sum3 + i3; 
           i3++;
        }while(i3 <= 1000);
        
        System.out.printf("%d\n", sum);
        System.out.printf("%d\n", sum2);
        System.out.printf("%d\n", sum3);
    }
    
    /**Exercise:
    * How many times is the following loop body repeated? What is the printout of the loop?  
    * int i = 1;
    * while( i < 10 )
    *   if ( (i++) % 2 == 0 )
    *   System.out.println( i );
    */
    public static void curs3_ex2(){
        int i = 1;
        while( i < 10 ){
            if ( (i++) % 2 == 0 ){
                System.out.println( i );
            }
        }
    }

    /**Exercise:
     * Write a valid java statement for the following item:Output only the positive numbers from x up to y.
     * Use the Scanner class to read  the values for the variables.
     */
    public static void curs3_ex3(){
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the first number:");
        int number1 = input.nextInt();

        System.out.println("Enter the second number:");
        int number2 = input.nextInt();

        int min = 0;
        int max = 0;

        if(number1 > number2) {
            max = number1;
            min = number2;
        } else {
            max = number2;
            min = number1;
        }

        if(min != max){
            for(int i = min; i <= max; i++) {
                if(i > 0)
                    System.out.printf("%d ", i);
            }
        }else{
            System.out.print("Numbers are equal, please use different numbers to show range");
        }

    }

    /**Exercise:
     * Write a valid java statement for the following item:Output the numbers from y down to 0.
     * Use the Scanner class to read the values  for the variables.
     */
    public static void curs3_ex4(){
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the number:");
        int number = input.nextInt();

        if(number > 0){
            for(int i = number; i >= 0; i--) {
                System.out.printf("%d ", i);
            }
        }else{
            System.out.print("Number should be greater than 0");
        }
    }

    /**Exercise:
     * Write a valid java statement for the following item:Output only the even numbers between the int x and int y.
     * Use the Scanner  class to read the values for the variables.
     */
    public static void curs3_ex5(){
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the first number:");
        int number1 = input.nextInt();

        System.out.println("Enter the second number:");
        int number2 = input.nextInt();

        int min = 0;
        int max = 0;

        if(number1 > number2) {
            max = number1;
            min = number2;
        } else {
            max = number2;
            min = number1;
        }

        if(min != max){
            for(int i = min; i <= max; i++) {
                if(Curs2.isEven(i))
                    System.out.printf("%d ", i);
            }
        }else{
            System.out.print("Numbers are equal, please use different numbers to show range");
        }
    }

    /**Exercise:
     * Write a valid java statement for the following item: Output the average of all numbers between int x and int y.
     * Use the Scanner  class to read the values for the variables.
     */
    public static void curs3_ex6(){
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the first number:");
        double number1 = input.nextInt();

        System.out.println("Enter the second number:");
        double number2 = input.nextInt();

        double min = 0;
        double max = 0;

        if(number1 > number2) {
            max = number1;
            min = number2;
        } else {
            max = number2;
            min = number1;
        }

        if(min != max){
            double sum = 0;
            int count = 0;
            for(double i = min; i <= max; i++) {
                sum += i;
                count++;
            }

            double avg = sum / count;

            System.out.printf("Sum: %f\n", sum);
            System.out.printf("Count: %d\n", count);
            System.out.printf("The average is: %f\n", avg);
        }else{
            System.out.print("Numbers are equal, please use different numbers to show range");
        }
    }

    /**Exercise:
     * Assume that the variables x and y contain integers.
     * Write code to perform the following tasks.
     * A. Output the largest value, using an if statement.
     * B. Output the largest value, using the method Math.max
     */
    public static void curs3_ex7(){
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the first number:");
        int number1 = input.nextInt();

        System.out.println("Enter the second number:");
        int number2 = input.nextInt();

        int minA = 0;
        int maxA = 0;
        int minB = 0;
        int maxB = 0;


        // A
        if(number1 > number2) {
            maxA = number1;
            minA = number2;
        } else {
            maxA = number2;
            minA = number1;
        }

        // B
        maxB = Math.max(number1, number2);
        minB = (maxB == number1) ? number2 : number1;

        System.out.printf("A. Max: %d, Min: %d\n", maxA, minA);
        System.out.printf("B. Max: %d, Min: %d\n", maxB, minB);


    }

    /**Exercise:
     * Write a program which asks the user a number between 1 - 7.
     * Display the day of the week by using a switch statement. Use the  Scanner class for value input.
     */
    public static void curs3_ex8(){
        Scanner input = new Scanner(System.in);

        System.out.print("Enter the day number (between 1 and 7):");
        int dayNumber = input.nextInt();
        System.out.println();

        String dayName = "";
        if(dayNumber > 0 && dayNumber < 7){
            switch (dayNumber){
                case 1: dayName = "Monday"; break;
                case 2: dayName = "Tuesday"; break;
                case 3: dayName = "Wednesday"; break;
                case 4: dayName = "Thursday"; break;
                case 5: dayName = "Friday"; break;
                case 6: dayName = "Saturday"; break;
                case 7: dayName = "Sunday"; break;
            }
            System.out.printf("The day is %s", dayName);
        }else{
            System.out.println("Not a valid day number");
        }

    }

    /**Exercise:
     * Print out the indexes of a matrix using nested 2 “for” loops.
     * Dimension should be inserted using the Scanner class.
     * Try following  versions:
     *  a. Number of lines = number of columns
     *  b. Number of lines != number of columns
     */
    public static void curs3_ex9(){
        Scanner input = new Scanner(System.in);

        System.out.print("Enter the matrix matrix dimension: ");
        int dimention = input.nextInt();
        System.out.println();

        for (int i = 1; i < dimention + 1; i++) {
            for (int j = 1; j < dimention + 1; j++) {
                System.out.printf("%2d ", i*j);
            }
            System.out.println();
        }

        System.out.println();

        for (int i = 1; i < dimention + 2; i++) {
            for (int j = 1; j < dimention; j++) {
                System.out.printf("%2d ", i*j);
            }
            System.out.println();
        }
    }

    /**
     * Exercise:
     * Write a program to produce the multiplication table of 1 to 9 as shown using two nested for-loops
     */
    public static void curs3_ex10(){
        final int GRID_SIZE = 9;
        for (int i = 0; i < GRID_SIZE + 1; i++) {
            if(i == 0){
                System.out.printf("%2s|", "*");
            }else{
                System.out.printf("%2d ", i);
            }
        }
       
        System.out.println();

        for (int i = 0; i < GRID_SIZE + 1; i++) {
            System.out.printf("%2s-", "--");
        }

        System.out.println();

        for (int i = 1; i < GRID_SIZE + 1; i++) {
            for (int j = 0; j < GRID_SIZE + 1; j++) {
                if(j == 0){
                    System.out.printf("%2d|", i);
                }else{
                    System.out.printf("%2d ", i*j);
                }
            }
            System.out.println();
        }
    }
}
