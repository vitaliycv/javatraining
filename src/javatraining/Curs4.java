/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatraining;

import java.util.Arrays;

/**
 *
 * @author Vitaliy
 */
public class Curs4 {

    private static final int CURS_NUMBER = 4;

    public Curs4(){
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 1);
        curs4_ex1();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 2);
        curs4_ex2();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 3);
        curs4_ex3();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 4);
        curs4_ex4();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 5);
        curs4_ex5();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 6);
        curs4_ex6();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 7);
        curs4_ex7();
    }

    /**
     * Exercise:
     * Whether a multidimensional array with the following notes.
     * Compute the average of each student.
     * Calculate the average for each domain.
     */
    public static void curs4_ex1(){
        String[] domains = {"", "Physics", "Maths", "History", "Geography"};
        String[] students = {"", "George", "Andrei", "Monica", "Vlad", "Camelia"};
        String[][] studentToDomain = {
                domains,
                {students[1], "10", "8", "6", "10"},
                {students[2], "7", "9", "7", "8"},
                {students[3], "8", "8", "10", "6"},
                {students[4], "7", "7", "9", "8"},
                {students[5], "9", "10", "7", "9"}
        };


        double[] sumStudentValues = new double[students.length+1];
        double[] sumDomainValues = new double[domains.length+1];


        for(int i = 0; i < students.length; i++){

            double sumByStudent = 0;

            for(int j = 0; j < domains.length; j++){
                if(j == 0){System.out.println();}
                System.out.printf("%9s ", studentToDomain[i][j]);
                if(i > 0 && j > 0){
                    sumByStudent += Integer.parseInt(studentToDomain[i][j]);
                    sumDomainValues[j] += Integer.parseInt(studentToDomain[i][j]);

                }
            }

            if(i > 0){
                sumStudentValues[i] = sumByStudent;
            }

        }

        System.out.println();
        System.out.println();

        for(int i = 1; i < students.length; i++){
            double avg = sumStudentValues[i] / (domains.length-1);
            System.out.printf("%7s's average : %f\n", students[i], avg);
        }

        System.out.println();
        System.out.println();

        for(int i = 1; i < domains.length; i++){
            double avg = sumDomainValues[i] / (students.length-1);
            System.out.printf("%7s average : %f\n", domains[i], avg);
        }
    }

    /**
     * Exercise:
     * Output the following models:
     * Hints: On the diagonal, row = col.  On the opposite diagonal, row + col = size + 1.
     */
    public static void curs4_ex2(){
        modelA(8);
        modelB(8);
        modelC(8);
        modelD(8);
    }

    public static void modelA(int size) {
        int i, j;
        for (i = 0; i < size; i++) {
            for (j = 0; j <= i; j++) {
                System.out.print("# ");
            }
            System.out.println();
        }
        System.out.println(" --(a)-- ");
    }

    public static void modelB(int size) {
        int i, j;
        for (i = 0; i < size; i++) {
            for (j = 0; j <= size; j++) {
                if(i < j ){
                    System.out.print("# ");
                }
            }
            System.out.println();
        }
        System.out.println(" --(b)-- ");
    }

    public static void modelC(int size) {
        int i, j, k;
        for(i = size; i >= 1; i--) {
            for(j = 1; j < i; j++) {
                System.out.print("  ");
            }
            for(k = size; k >= i; k--) {
                System.out.print("# ");
            }
            System.out.println();
        }
        System.out.println(" --(c)-- ");
        System.out.println();
    }

    public static void modelD(int size) {
        int i, j, k;
        for(i = size; i >= 1;i--)
        {
            for(j = size; j > i; j--)
            {
                System.out.print("  ");
            }
            for(k = 1; k <= i; k++)
            {
                System.out.print("# ");
            }
            System.out.println();
        }
        System.out.println(" --(d)-- ");
        System.out.println();
    }

    /**
     * Exercise:
     */
    public static void curs4_ex3(){
        modelE(7);
        modelF(7);
        modelG(7);
        modelH(7);
        modelI(7);
    }
    
    public static void modelE(int size) {
        int i, j, k;
        for(i = size; i >= 1;i--)
        {
            for(j = size; j >= 1; j--)
            {
                if(i == size || j == size || i == 1 || j == 1){
                    System.out.print("# ");
                }else{
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        System.out.println(" --(e)-- ");
        System.out.println();
    }

    public static void modelF(int size) {
        int i, j, k;
        for(i = size; i >= 1;i--)
        {
            for(j = size; j >= 1; j--)
            {
                if(i == size || i == 1 || i == j){
                    System.out.print("# ");
                }else{
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        System.out.println(" --(f)-- ");
        System.out.println();
    }

    public static void modelG(int size) {
        int i, j, k;
        for(i = size; i >= 1;i--)
        {
            for(j = size; j >= 1; j--)
            {
                if(i == size || i == 1 || j == size-i+1){
                    System.out.print("# ");
                }else{
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        System.out.println(" --(g)-- ");
        System.out.println();
    }

    public static void modelH(int size) {
        int i, j, k;
        for(i = size; i >= 1;i--)
        {
            for(j = size; j >= 1; j--)
            {
                if(i == size || i == 1 || j == size-i+1 || i == j){
                    System.out.print("# ");
                }else{
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        System.out.println(" --(h)-- ");
        System.out.println();
    }

    public static void modelI(int size) {
        int i, j, k;
        for(i = size; i >= 1;i--)
        {
            for(j = size; j >= 1; j--)
            {
                if(i == size || i == 1 || j == size-i+1 || i == j || j == size || j == 1){
                    System.out.print("# ");
                }else{
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        System.out.println(" --(i)-- ");
        System.out.println();
    }

    /**
     * Exercise:
     */
    public static void curs4_ex4(){
        modelJ(6);
        modelK(6);
        modelL(6);
    }

    public static void modelJ(int size) {
        int i, j, k;
        for( i = size; i >= 1; i--) {
            for(j=size;j>i;j--) {
                System.out.print("  ");
            }
            for( k = 1; k < (i*2); k++) {
                System.out.print("# ");
            }
            System.out.println();
        }
        System.out.println(" --(j)-- ");
        System.out.println();
    }

    public static void modelK(int size) {
        modelKL(size);
        System.out.println(" --(k)-- ");
        System.out.println();
    }

    public static void modelKL(int size) {
        int i, j, k;
        for(i = 1; i <= size; i++) {
            for(j = i; j < size; j++) {
                System.out.print("  ");
            }
            for( k = 1; k < (i*2); k++) {
                System.out.print("# ");
            }
            System.out.println();
        }
    }

    public static void modelL(int size) {
        int i, j, k;

        modelKL(size);

        for(i = size-1; i>=1; i--) {
            for(j = size; j > i; j--) {
                System.out.print("  ");
            }
            for(k=1;k<(i*2);k++) {
                System.out.print("# ");
            }
            System.out.println();
        }

        System.out.println(" --(l)-- ");
        System.out.println();
    }

    /**
     * Exercise:
     */
    public static void curs4_ex5(){
        modelM(8);
        modelN(8);
        modelO(8);
        modelP(8);
    }

    public static void modelM(int size){
        for (int i = 1; i <= size; i++)
        {
            for (int j = 1; j <= i; j++) {
                System.out.printf("%d ", j);
            }
            System.out.println();
        }
        System.out.println(" --(m)-- ");
        System.out.println();
    }

    public static void modelN(int size) {
        int i, j;
        for (i = 0; i < size; i++) {
            for (j = 0; j <= size; j++) {
                if(i < j ){
                    System.out.printf("%d ", j-i);
                }else{
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        System.out.println(" --(n)-- ");
        System.out.println();
    }

    public static void modelO(int size) {
        int i, j, k;
        for (i = 1; i <= size; i++) {
            for(j = size; j > i; j--) {
                System.out.print("  ");
            }
            for(k = i; k >= 1; k--) {
                System.out.printf("%d ", k);
            }
            System.out.println();
        }
        System.out.println(" --(o)-- ");
        System.out.println();
    }

    public static void modelP(int size) {
        int i, j, k;
        for (i = 1; i <= size; i++) {
            for(j = size; j >= i; j--) {
                System.out.printf("%d ", j-i+1);
            }
            for(k = i; k >= 1; k--) {
                System.out.print("  ");
            }
            System.out.println();
        }
        System.out.println(" --(p)-- ");
        System.out.println();
    }

    /**
     * Exercise:
     */
    public static void curs4_ex6(){
        modelQ(8);
        modelR(8);
    }

    public static void modelQ(int size) {
        int i, j, k, m;
        for(i = 1; i <= size; i++) {
            for(j = i; j < size; j++) {
                System.out.print("  ");
            }
            for( k = 1; k < (i*2); k++) {
                if(k <= i){
                    System.out.printf("%d ", k);
                }
            }
            for( m = i-1; m >= 1; m--) {
                System.out.printf("%d ", m);
            }
            System.out.println();
        }
        System.out.println(" --(q)-- ");
        System.out.println();
    }

    public static void modelR(int size) {
        int i, j, k, m;
        for( i = size; i >= 1; i--) {
            for(j = size; j > i; j--) {
                System.out.print("  ");
            }
            for( k = 1; k < (i*2); k++) {
                if(k <= i) {
                    System.out.printf("%d ", k);
                }
            }
            for( m = i-1; m >= 1; m--) {
                System.out.printf("%d ", m);
            }
            System.out.println();
        }
        System.out.println(" --(r)-- ");
        System.out.println();
    }

    /**
     * Exercise:
     */
    public static void curs4_ex7(){
        modelS(8);
        modelT(8);
    }

    public static void modelS(int size) {
        int i, j, k, m;
        for (i = 1; i <= size; i++)
        {
            for (j = 1; j <= i; j++) {
                if(j != size){
                    System.out.printf("%d ", j);
                }

            }
            for( k = (size-i)*2 ; k > 1; k--) {
                System.out.print("  ");
            }
            for( m = i; m >= 1; m--) {
                System.out.printf("%d ", m);
            }
            System.out.println();
        }
        System.out.println(" --(s)-- ");
        System.out.println();
    }

    public static void modelT(int size) {
        int i, j, k, m;

        for (i = 1; i <= size; i++)
        {
            for(j = 1; j <= size-1; j++) {
                if(j > size-i+1){
                    System.out.print("  ");
                }else {
                    System.out.printf("%d ", j);
                }

            }
            for( k = 1 ; k < i; k++) {
                System.out.print("  ");
            }

            for( m = size-i+1; m >= 1; m--) {
                System.out.printf("%d ", m);
            }

            System.out.println();
        }
        System.out.println(" --(t)-- ");
        System.out.println();
    }
}
