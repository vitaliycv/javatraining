/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatraining;

/**
 *
 * @author Vitaliy
 */
public class Curs1 {
    
    private static final int CURS_NUMBER = 1;
    
    public Curs1(){
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 1);
        curs1_ex1();
        
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 2);
        curs1_ex2();
        
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 3);
        curs1_ex3();
    }
    /**
     * Exercise 1:
    What do the following statements display?  
    *   System.out.println("\ta\tb\tc");  
    *   System.out.println("\\\\");  
    *   System.out.println("'");
    *   System.out.println("\"\"\"");
     */
    public static void curs1_ex1() {
        System.out.println("\ta\tb\tc");  
        System.out.println("\\\\");  
        System.out.println("'");
        System.out.println("\"\"\"");
    }
    
    /**
     * Exercise:
     * Write a println statement to display the following output in the console:
     * / \ // \\ /// \\\
     */
    public static void curs1_ex2(){
        System.out.println("/ \\ // \\\\ /// \\\\\\");
    }
    
    /** Exercise:
     * Write a println statement to display the following output in the console:
     * D:\norbert\tutorialJava\JavaTutorial.pdf
     */
    public static void curs1_ex3(){
        System.out.println("D:\\norbert\\tutorialJava\\JavaTutorial.pdf");
    }

    
}


