package javatraining;
import java.util.Scanner;
/**
 * Created by vsiukayev on 11-Oct-17.
 */
public class Curs5 {

    private static final int CURS_NUMBER = 5;
    public static final int GENDER_MALE = 1;
    public static final int GENDER_FEMALE = 2;

    public Curs5(){
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 1);
        curs5_ex1();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 2);
        curs5_ex2();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 3);
        curs5_ex3();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 4);
        curs5_ex4();
    }

    /**
     * Exercise:
     * Write a method that calculates the average of three numbers.
     * Display the result in the console.
     * The three numbers are introduced via the keyboard.
     */
    public static void curs5_ex1(){
        Scanner input = new Scanner(System.in);

        System.out.print("\nEnter the first number: ");
        double number1 = input.nextInt();

        System.out.print("\nEnter the second number: ");
        double number2 = input.nextInt();

        System.out.print("\nEnter the third number: ");
        double number3 = input.nextInt();

        System.out.println();
        System.out.printf("Average is: %f", averageOfThree(number1, number2, number3));
        System.out.println();
    }

    public static double averageOfThree(double num1, double num2, double num3){
        return (num1 + num2 + num3) / 3;
    }

    /**
     * Exercise:
     * Write a method that receives three arguments: name, height, gender and age.
     * The method calculates and returns the ideal weight.
     * Check ideal weight for 4 people.
     * Hint:
     * Ideal weight male = (height - 100 - ((height - 150) / 4) + ((age - 20) / 4)
     * Ideal weight female = (height - 100 -  ((height - 150) / 2) + ((age - 20) / 6)
     */
    public static void curs5_ex2(){

        System.out.printf("%7s, your ideal weight should be %f\n", "Vlad", getIdealWeight(1, 154, 11));
        System.out.printf("%7s, your ideal weight should be %f\n", "Camilla", getIdealWeight(2, 162, 26));
        System.out.printf("%7s, your ideal weight should be %f\n", "Sam", getIdealWeight(1, 174, 31));
        System.out.printf("%7s, your ideal weight should be %f\n", "Mary", getIdealWeight(2, 168, 21));

        System.out.println();


    }

    public static double getIdealWeight(int gender, double height, int age) {
        double idealWeight = 0;
        switch (gender) {
            case GENDER_MALE:
                idealWeight = (height - 100 - ((height - 150) / 4) + ((age - 20) / 4));
                break;
            case GENDER_FEMALE:
                idealWeight = (height-100-((height-150)/2)+((age-20)/6));
                break;
            default: idealWeight = 0;
        };
        return idealWeight;
    }

    /**
     * Exercise:
     * Whether following strings:
     * {7, 5, 2 1, 3};
     * {13, 2, 2, 5, 3, 4}
     * {10, 2, 2, 4, 5}
     * {1,3,7}
     * Write a method to calculate the multiplication of the elements from an array.
     * Reuse method.
     */
    public static void curs5_ex3(){
        int[] numbers1 = {7, 5, 2, 1, 3};
        int[] numbers2 = {13, 2, 2, 5, 3, 4};
        int[] numbers3 = {10, 2, 2, 4, 5};
        int[] numbers4 = {1,3,7};
        System.out.printf("Multiplication result 1: %d\n", getValuesMultiplication(numbers1));
        System.out.printf("Multiplication result 2: %d\n", getValuesMultiplication(numbers2));
        System.out.printf("Multiplication result 3: %d\n", getValuesMultiplication(numbers3));
        System.out.printf("Multiplication result 4: %d\n", getValuesMultiplication(numbers4));
    }

    public static int getValuesMultiplication(int[] numbers) {
        int result = 1;
        for (int num : numbers){
            result *= num;
        }
        return result;
    }

    /**
     * Exercise:
     * Write a method that receives two parameters - width and height.
     */
    public static void curs5_ex4(){
        drawRectangle(9, 3);
        drawRectangle(7, 5);
    }

    public static void drawRectangle(int width, int height) {
        int i, j;
        for(i = 1; i <= height; i++) {
            for(j = 1; j <= width; j++) {
                if( j == width || i == height || i == 1 || j == 1){
                    System.out.print("* ");
                }else{
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

}
