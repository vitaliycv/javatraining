package javatraining;

/**
 * Created by vsiukayev on 11-Oct-17.
 */
public class Curs6 {

    private static final int CURS_NUMBER = 6;

    public Curs6(){
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 1);
        curs6_ex1();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 2);
        curs6_ex2();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 3);
        curs6_ex3();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 4);
        curs6_ex4();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 5);
        curs6_ex5();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 6);
        curs6_ex6();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 7);
        curs6_ex7();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 8);
        curs6_ex8();
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 9);
        curs6_ex9();
    }

    /**
     * Exercise:
     * The String class’s charAt method returns a primitive char value from a specified int index value in
     * relationship to the referenced  string object.
     * + charAt(int) : char
     */
    public static void curs6_ex1(){
        String pirateMessage = "  Buried Treasure Chest! ";
        /* Returns the 'blank space' character from location 0 */
        char c1 = pirateMessage.charAt(0);
        /* Returns the character 'B' from location 2 */
        char c2 = pirateMessage.charAt(2);
        /* Returns the character '!' from location 23 */
        char c3 = pirateMessage.charAt(23);
        /* Returns the 'blank space' character from location 24 */
        char c4 = pirateMessage.charAt(24);
        System.out.printf("c1: %s\n", c1);
        System.out.printf("c2: %s\n", c2);
        System.out.printf("c3: %s\n", c3);
        System.out.printf("c4: %s\n", c4);

    }


    /**
     * Exercise:
     * The String class’s indexOf methods return primitive int values representing the index of a character or string
     * in relationship to the  referenced string object.
     * + indexOf(int) : int
     * + indexOf(int, int) : int
     * + indexOf(String) : int
     * + indexOf(String, int) : int
     */
    public static void curs6_ex2(){
        String pirateMessage = "  Buried Treasure Chest! ";
        /* Returns the integer 3 as it is the first 'u' in the string. */
        int i1 = pirateMessage.indexOf('u'); // 3
        /* Returns the integer 14 as it is the first 'u' in the string past location 9. */
        int i2 = pirateMessage.indexOf('u', 9); // 14
        /* Returns the integer 13 as it starts at location 13 in the string. */
        int i3 = pirateMessage.indexOf("sure"); // 13
        /* Returns the integer -1 as there is no Treasure string on or past location 10 */
        int i4 = pirateMessage.indexOf("Treasure", 10); // -1!
        /* Returns the integer -1 as there is no character u on or past location 100 */
        int i5 = pirateMessage.indexOf("u", 100); // -1!

        System.out.printf("i1: %s\n", i1);
        System.out.printf("i2: %s\n", i2);
        System.out.printf("i3: %s\n", i3);
        System.out.printf("i4: %s\n", i4);
        System.out.printf("i5: %s\n", i4);

    }

    /**
     * Exercise:
     * The String class’s length method returns a primitive int value representing the length
     * of the referenced string  object.
     * + length() : int
     */
    public static void curs6_ex3(){
        String pirateMessage = "  Buried Treasure Chest! ";
        int i = pirateMessage.length(); // 25
        System.out.printf("i: %s\n", i);
    }

    /**
     * Exercise:
     * The String class’s startsWith method returns a primitive boolean value representing the results of a test to
     * see if  the supplied prefix starts the referenced String object.
     * + startsWith(String, int) : boolean
     * + startsWith(String) : boolean
     */
    public static void curs6_ex4(){
        String pirateMessage = "  Buried Treasure Chest! ";
        /* Returns true as the referenced string starts with the compared string. */
        boolean b1 = pirateMessage.startsWith("  Buried Treasure"); // true
        /* Returns false as the referenced string does not start with the compared string. */
        boolean b2 = pirateMessage.startsWith(" Discovered"); // false
        /* Returns false as the referenced string does not start with the compared string at location 8. */
        boolean b3 = pirateMessage.startsWith("Treasure", 8); // false
        /* Returns true as the referenced string does start with the compared string at location 9. */
        boolean b4 = pirateMessage.startsWith("Treasure", 9); // true

        System.out.printf("b1: %s\n", b1);
        System.out.printf("b2: %s\n", b2);
        System.out.printf("b3: %s\n", b3);
        System.out.printf("b4: %s\n", b4);
    }

    /**
     * Exercise:
     * The String class’s endsWith method returns a primitive boolean value representing the results of a test to
     * see if  the supplied suffix ends the referenced string object.
     * + endsWith(String) : boolean
     */
    public static void curs6_ex5(){
        String pirateMessage = "  Buried Treasure Chest! ";

        /* Returns true as the referenced string ends with the compared string. */
        boolean b1 = pirateMessage.endsWith("Treasure Chest! "); // true
        /* Returns false as the referenced string does not end with the compared string. */
        boolean b2 = pirateMessage.endsWith("Treasure Chest "); // false

        System.out.printf("b1: %s\n", b1);
        System.out.printf("b2: %s\n", b2);
    }

    /**
     * Exercise:
     * The String class’s substring methods return new strings that are substrings of the referenced string object.
     * + substring(int) : String
     * + substring(int, int) : String

     */
    public static void curs6_ex6(){
        String pirateMessage = "  Buried Treasure Chest! ";
        String s1 = "";
        String s2 = "";
        String s3 = "";
        String s4 = "";
        String s5 = "";
        try{
            /* Returns the entire string starting at index 9. */
            s1 = pirateMessage.substring(9); // Treasure Chest!
            /* Returns the string at index 9. */
            s2 = pirateMessage.substring(9, 10); // T
            /* Returns the string at index 9 and ending at index 23. */
            s3 = pirateMessage.substring(9, 23); // Treasure Chest
            /* Produces runtime error. */
            s4 = pirateMessage.substring(9, 8); // String index out of range: -1
            /* Returns a blank */
            s5 = pirateMessage.substring(9, 9); // Blank

        }catch (Exception e){
            System.out.printf(e.getMessage());
            System.out.println();
        }
        System.out.printf("s1: %s\n", s1);
        System.out.printf("s2: %s\n", s2);
        System.out.printf("s3: %s\n", s3);
        System.out.printf("s4: %s\n", s4);
        System.out.printf("s5: %s\n", s5);
    }

    /**
     * Exercise:
     * The String class’s trim method returns the entire string minus leading and trailing whitespace characters in
     * relationship to the referenced string object.
     * + trim() : String
     */
    public static void curs6_ex7(){
        String pirateMessage = "  Buried Treasure Chest! ";
        /* "Buried Treasure Chest!" with no leading or trailing white spaces */
        String s = pirateMessage.trim();

        System.out.printf("s: %s\n", s);
    }

    /**
     * Exercise:
     *
     * Java allows for methods to be chained together. Consider the following message:
     * String msg = " The night is dark and full of terrors! ";
     * We wish to change the message to read: “The cheeseburger is juicy and stuffed with bacon.”
     *
     * Three changes need to be made to adjust the string as desired:
     * 1. Trim the leading and trailing whitespace.
     * 2. Replace the substring “night” with “cheeseburger”.
     * 3. Replace the substring “full of terrors” with “stuffed with bacon”.
     * 4. Add a period at the end of the sentence.
     */
    public static void curs6_ex8(){
        String msg = " The night is dark and full of terrors! ";
        String trimmedMsg = msg.trim();
        trimmedMsg = trimmedMsg.replace("night", "cheeseburger");
        trimmedMsg = trimmedMsg.replace("dark", "juicy");
        trimmedMsg = trimmedMsg.replace("full of terrors", "stuffed with bacon");
        trimmedMsg = trimmedMsg.replace("!", ".");

        System.out.println(trimmedMsg);
    }

    /**
     * Exercise:
     * String tenCharString = "AAAAAAAAAA";
     * System.out.println(tenCharString.replace("AAA", "LLL"));
     * What is printed to the standard out?
     */
    public static void curs6_ex9(){
        String tenCharString = "AAAAAAAAAA";
        System.out.println(tenCharString.replace("AAA", "LLL"));

        //Answer: LLLLLLLLLA
    }

}
