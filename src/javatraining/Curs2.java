/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatraining;
import java.util.Scanner;
/**
 *
 * @author Vitaliy
 */
public class Curs2 {
    
    private static final int CURS_NUMBER = 2;
    
    public Curs2(){
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 1);
        curs2_ex1();
        
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 2);
        curs2_ex2();
        
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 3);
        curs2_ex3();
        
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 4);
        curs2_ex4();
        
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 5);
        curs2_ex5();
        
        System.out.printf("\nCurs %d exercise %d:\n", CURS_NUMBER, 6);
        curs2_ex6();
    }
    
    /** Exercise:
     * How do you check if a number is even/odd?
     */
    public static void curs2_ex1(){
        System.out.printf("Is even 15: %b\n", isEven(15));
        System.out.printf("Is even 10: %b\n", isEven(10));
        System.out.printf("Is odd 10: %b\n", isOdd(10));
        System.out.printf("Is odd 15: %b\n", isOdd(15));
    }
    
    public static Boolean isEven(int number){
        return (number % 2 == 0);
    }
    
    public static Boolean isOdd(int number){
        return !isEven(number);
    }
    
    /**Exercise:
     * Are these two variables (result1 and result2) of int type equals?   
     * int result1 = 100 * 100 / 5 + 200 * 3 / 2;
     * int result1 = 100 * 100 / (5 + 200) * 3 / 2;
     */
    public static void curs2_ex2(){
        int result1 = 100 * 100 / 5 + 200 * 3 / 2;
        int result2 = 100 * 100 / (5 + 200) * 3 / 2;
        System.out.printf("Is %d == %d: %b", result1, result2, result1 == result2);
        System.out.println();
    }
    
    /**
     * Exercise :
     * Calculate the area of a triangle. 
     * Use the Scanner class in order to introduce side lengths.
    */
    public static void curs2_ex3(){
      Scanner input = new Scanner(System.in);

      System.out.println("Enter the width of the Triangle:");
      double width = input.nextDouble();

      System.out.println("Enter the height of the Triangle:");
      double height = input.nextDouble();

      //Area = (width*height)/2
      double area = (width * height) / 2;
      System.out.println("Area of Triangle is: " + area);      
    }
    
    /** Exercise :
     *  Calculate the area of a trapezoid. 
     * Use Scanner class to be introduced side lengths.
    */
    public static void curs2_ex4(){
        double base1, base2, height, Area, Median; 
        Scanner input = new Scanner(System.in);

        System.out.println(" Please Enter First Base of a Trapezoid: ");
        base1 = input.nextDouble();
        System.out.println(" Please Enter Second Base of a Trapezoid: ");
        base2 = input.nextDouble();
        System.out.println(" Please Enter the Height of a Trapezoid: ");
        height = input.nextDouble();

        Area = 0.5 * (base1 + base2) * height;
        Median = 0.5 * (base1+ base2);

        System.out.format("\n The Area of a Trapezoid = %.2f\n",Area);
        System.out.format(" The Median of a trapezium = %.2f \n", Median);
    }
    
    /** Exercise :
     * Calculate the hypotenuse of a right triangle Pythagorean method. 
     * Use Scanner class to enter the lengths of the two catheters.
    */
    public static void curs2_ex5(){
        double a, b, c; 
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the width of the right Triangle: ");
        a = input.nextDouble();
        System.out.println("Enter the height of the right Triangle: ");
        b = input.nextDouble();
        c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        System.out.printf("The hypotenuse of a right triangle Pythagorean method = %f\n", c);
    }
    
    /** Exercise :
     * Write a program that reads four values from the keyboard.
     * Calculate the multiplication value between the maximum of the first two numbers 
     * and the minimum of the last two numbers.
    */
    public static void curs2_ex6(){
        int[] numbers = new int[4];
        
        Scanner input = new Scanner(System.in);

        System.out.println("Enter first number: ");
        numbers[0] = input.nextInt();
        
        System.out.println("Enter second number: ");
        numbers[1] = input.nextInt();
        
        System.out.println("Enter third number: ");
        numbers[2] = input.nextInt();
        
        System.out.println("Enter fourth number: ");
        numbers[3] = input.nextInt();
        
        int[] twoLargest = twoLargest(numbers);
        int[] twoSmallest = twoSmallest(numbers);
        int multiplicationOfTwoLagest = twoLargest[0] * twoLargest[1];
        int multiplicationOfTwoSmallest = twoSmallest[0] * twoSmallest[1];
        System.out.printf("Two Largest: %d, %d\n", twoLargest[0], twoLargest[1]);
        System.out.printf("Two Smallest: %d, %d\n", twoSmallest[0], twoSmallest[1]);
        System.out.printf("The multiplication value between the maximum of the first two numbers: %d\n", multiplicationOfTwoLagest);
        System.out.printf("The multiplication value between the minimum of the last two numbers: %d\n", multiplicationOfTwoSmallest);
    }
    
    public static int[] twoLargest(int values[]){
        int largestA = Integer.MIN_VALUE, largestB = Integer.MIN_VALUE;

        for(int value : values) {
            if(value > largestA) {
                largestB = largestA;
                largestA = value;
            } else if (value > largestB) {
                largestB = value;
            }
        }
        return new int[] { largestA, largestB }; 
    }
    
    public static int[] twoSmallest(int values[]){
        int smallestA = Integer.MAX_VALUE, smallestB = Integer.MAX_VALUE;

        for(int value : values) {
            if(value < smallestA) {
                smallestB = smallestA;
                smallestA = value;
            } else if (value < smallestB) {
                smallestB = value;
            }
        }
        return new int[] { smallestA, smallestB }; 
    }
}
